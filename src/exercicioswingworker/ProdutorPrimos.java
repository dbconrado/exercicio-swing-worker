/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicioswingworker;

/**
 * Um objeto dessa classe produz números primos começando de 1.
 * 
 * 
 * @author dbconrado
 */
public class ProdutorPrimos {

    private int primoAtual = 0;
    
    /**
     * Encontra o primeiro número primo imediatamente depois do último número
     * primo encontrado.
     * 
     * @return o número primo encontrado.
     */
    public int proximo() {
        boolean isPrimo = false;
        
        while (!isPrimo) {
            primoAtual++;
            isPrimo = true; // é primo até que se prove o contrário
            
            for (int i = 2; i <= primoAtual/2; i++) {
                if (primoAtual % i == 0) {
                    isPrimo = false;
                    break;
                }
            }
        }
        
        return primoAtual;
    }
}

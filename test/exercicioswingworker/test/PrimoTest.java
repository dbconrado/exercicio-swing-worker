/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicioswingworker.test;

import exercicioswingworker.ProdutorPrimos;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dbconrado
 */
public class PrimoTest {
    
    private ProdutorPrimos produtor;
    
    public PrimoTest() {
    }
    
    @Before
    public void setUp() {
        produtor = new ProdutorPrimos();
    }
    
    @Test
    public void testUmEhPrimo() {
        assertEquals(1, produtor.proximo());
    }
    
    @Test
    public void testDoisEhPrimo() {
        produtor.proximo();
        assertEquals(2, produtor.proximo());
    }
    
    @Test
    public void testTresEhPrimo() {
        produtor.proximo();
        produtor.proximo();
        assertEquals(3, produtor.proximo());
    }
    
    @Test
    public void testeDepoisDeTresVemCinco() {
        produtor.proximo(); // 1
        produtor.proximo(); // 2
        produtor.proximo(); // 3
        assertEquals(5, produtor.proximo());
    }
    
    @Test
    public void testeDepoisDeCincoVemSeteOnzeTreze() {
        produtor.proximo(); // 1
        produtor.proximo(); // 2
        produtor.proximo(); // 3
        produtor.proximo(); // 3
        assertEquals(7, produtor.proximo());
        assertEquals(11, produtor.proximo());
        assertEquals(13, produtor.proximo());
    }
    
}
